import { mount } from "enzyme";

import React from "react";

import Button from "./index";

describe("Button", () => {
    it("Should have an icon", () => {
        const button = mount(<Button icon="icon" />);
        expect(button.html()).toContain("icon");
    });

    it("Should not be clickable if disabled", () => {
        const mockOnClick = jest.fn();
        const button = mount(<Button disabled onClick={mockOnClick} />);
        button.find("button").simulate("click");
        expect(mockOnClick.mock.calls.length).toEqual(0);
    });
});
