import React from "react";

import { StyledButton } from "./Button.styles";

const Button = ({ icon, children, disabled, onClick = () => {} }) => (
    <StyledButton onClick={onClick} disabled={disabled}>
        {icon && (
            <span role="img" alt="woman raising hand" aria-label="woman raising hand">
                {icon}
            </span>
        )}
        {children}
    </StyledButton>
);

export default Button;
