import styled from "styled-components";

export const StyledButton = styled.button`
    background: transparent;
    border: 1px solid #eee;
    border-radius: 5px;
    color: #eee;
    padding: 0.5em;
    cursor: ${props => (props.disabled ? "default" : "pointer")};
    opacity: ${props => (props.disabled ? "0.7" : "1")};
    pointer-events: ${props => (props.disabled ? "none" : "all")};

    & + & {
        margin-left: 20px;
    }
`;
