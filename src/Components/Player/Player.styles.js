import styled from "styled-components";

import { Card } from "../../Styles/Styled";

export const PlayerHand = styled.div`
    background: #888;
    padding: 10px;
    border-radius: 5px;
    min-height: 55px;

    ${Card} {
        margin: 5px;
    }
`;

export const PlayerContainer = styled.article`
    padding: 10px;
`;

export const EditablePlayerName = styled.input`
    margin-right: 20px;
    background: transparent;
    color: white;
    border: none;
    border-bottom: 1px solid white;
    margin: 15px 0;
    margin-right: 20px;
`;

export const PlayerName = styled.p`
    margin-right: 20px;
`;
