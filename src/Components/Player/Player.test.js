import { mount } from "enzyme";

import React from "react";

import Player from "./index";
import { Card } from "../../Styles/Styled";

const defaultCards = [
    { value: "2", suit: "D" },
    { value: "T", suit: "S" },
    { value: "J", suit: "S" },
    { value: "T", suit: "H" },
    { value: "3", suit: "S" }
];

describe("Player", () => {
    it("Should render 5 cards", () => {
        const player = mount(<Player cards={defaultCards} />);
        expect(player.find(Card)).toHaveLength(5);
    });

    it("Should be editable", () => {
        const player = mount(<Player editMode />);
        expect(player.html()).toContain("input");
    });
});
