import React from "react";

import Button from "../Button";

import { PlayerContainer, PlayerHand, EditablePlayerName, PlayerName } from "./Player.styles";
import { Card, InlineRow } from "../../Styles/Styled";

const Player = ({
    name = "",
    cards = [],
    canDeletePlayer = true,
    editMode = false,
    deletePlayer = () => {},
    updatePlayerName = () => {},
    setEditMode = () => {}
}) => (
    <PlayerContainer>
        <InlineRow>
            {editMode ? (
                <EditablePlayerName value={name} onChange={e => updatePlayerName(e.target.value)} />
            ) : (
                <PlayerName>{name}</PlayerName>
            )}

            <Button icon="✏️" onClick={setEditMode}>
                Edit
            </Button>
            <Button icon="🔥" onClick={deletePlayer} disabled={!canDeletePlayer}>
                Remove
            </Button>
        </InlineRow>
        <PlayerHand>
            {cards.map(({ suit, value }) => (
                <Card key={suit + value} suit={suit}>
                    {value}
                </Card>
            ))}
        </PlayerHand>
    </PlayerContainer>
);

export default Player;
