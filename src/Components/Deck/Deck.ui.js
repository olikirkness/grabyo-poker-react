import React, { Fragment } from "react";

import { Card } from "../../Styles/Styled";
import { StyledDeck } from "./Deck.styles";

import { values, suits, drawnCardsDefault } from "../../utils";

const Deck = ({ drawnCards = drawnCardsDefault }) => (
    <StyledDeck>
        {suits.map(suit => (
            <Fragment key={suit}>
                {values.map(value => (
                    <Card
                        key={suit + value}
                        suit={suit}
                        selected={drawnCards[suit] && drawnCards[suit][value]}
                    >
                        {value}
                    </Card>
                ))}
            </Fragment>
        ))}
    </StyledDeck>
);

export default Deck;
