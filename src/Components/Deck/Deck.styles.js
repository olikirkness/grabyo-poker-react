import styled from "styled-components";

import { Card } from "../../Styles/Styled";

export const StyledDeck = styled.section`
    display: grid;
    grid-template-columns: repeat(13, 1fr);
    grid-template-rows: repeat(4, 1fr);
    text-align: center;

    ${Card} {
        margin: 10px auto;
    }
`;
