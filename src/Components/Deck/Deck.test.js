import { mount } from "enzyme";

import React from "react";

import Deck from "./index";
import { Card } from "../../Styles/Styled";

import { drawnCardsDefault } from "../../utils";

describe(`Card deck`, () => {
    test("renders the right amount of cards", () => {
        const deck = mount(<Deck drawnCards={drawnCardsDefault} />);
        expect(deck.find(Card)).toHaveLength(52);
    });
});
