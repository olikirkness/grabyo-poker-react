import React, { Component } from "react";
import poker from "poker-hands";

import { suits, values, cardsPerPlayer, maxPlayers, minPlayers, drawnCardsDefault } from "../utils";

import Layout from "./Layout";
import Deck from "./Deck";
import Player from "./Player";
import Button from "./Button";

import { Footer } from "../Styles/Styled";

class App extends Component {
    state = {
        drawnCards: drawnCardsDefault,
        playerEditMode: null,
        cards: values.reduce(
            (sum, value) => [
                ...sum,
                ...suits.map(suit => ({
                    value,
                    suit
                }))
            ],
            []
        ),
        players: [
            {
                name: "Player 1",
                cards: [],
                cardString: ""
            },
            {
                name: "Player 2",
                cards: [],
                cardString: ""
            }
        ]
    };

    componentDidMount = () => this.handleDrawCards();

    findWinner = () => {
        const { players } = this.state;

        const strings = players.map(player => player.cardString);

        const winnerIndex = poker.judgeWinner(strings);
        if (players[winnerIndex]) {
            alert(`${players[winnerIndex].name} wins`);
            this.handleDrawCards();
        }
    };

    handleDrawCards = () => {
        let { cards } = this.state;
        const { players } = this.state;

        const cardsToDraw = players.length * cardsPerPlayer;

        const drawnCards = [...Array(cardsToDraw)].map(() => {
            const index = Math.floor(Math.random() * cards.length);
            const card = cards[index];
            cards = [...cards].filter((_, i) => i !== index);

            return card;
        });

        this.setState({
            drawnCards: drawnCards.reduce(
                (sum, card) => ({
                    ...sum,
                    [card.suit]: {
                        ...(sum[card.suit] || {}),
                        [card.value]: true
                    }
                }),
                {}
            ),
            players: players.map((player, index) => {
                const cards = [...drawnCards].filter(
                    (_, i) =>
                        i >= index * cardsPerPlayer && i < index * cardsPerPlayer + cardsPerPlayer
                );

                const cardString = cards.map(({ value, suit }) => `${value}${suit}`).join(" ");
                return {
                    ...player,
                    cardString,
                    cards
                };
            })
        });
    };

    canAddNewPlayer = () => {
        const { players } = this.state;
        return players.length < maxPlayers;
    };

    canDeletePlayer = () => {
        const { players } = this.state;
        return players.length > minPlayers;
    };

    deletePlayer = index => {
        const { players } = this.state;
        if (this.canDeletePlayer()) {
            this.setState(
                {
                    players: players.filter((_, i) => i !== index)
                },
                this.handleDrawCards
            );
        }
    };

    addPlayer = () => {
        if (this.canAddNewPlayer()) {
            const { players } = this.state;

            this.setState(
                {
                    players: [
                        ...players,
                        {
                            name: `Player ${players.length + 1}`,
                            cards: [],
                            cardString: ""
                        }
                    ]
                },
                this.handleDrawCards
            );
        }
    };

    handleUpdatePlayerName = (name, index) => {
        const { players } = this.state;

        this.setState({
            players: players.map((player, i) => (i === index ? { ...player, name } : player))
        });
    };

    setEditMode = index => {
        const { playerEditMode } = this.state;
        this.setState({ playerEditMode: playerEditMode === index ? null : index });
    };

    render() {
        const { players, drawnCards, playerEditMode } = this.state;

        return (
            <Layout>
                <section>
                    <h1>Cards deck</h1>
                    <Deck drawnCards={drawnCards} />
                </section>
                <section>
                    <header>
                        <h1>Players</h1>
                    </header>
                    <section>
                        {players.map(({ name, cards, cardString }, index) => (
                            <Player
                                updatePlayerName={name => this.handleUpdatePlayerName(name, index)}
                                canDeletePlayer={this.canDeletePlayer()}
                                deletePlayer={() => this.deletePlayer(index)}
                                key={cardString || index}
                                name={name}
                                cards={cards}
                                editMode={index === playerEditMode}
                                setEditMode={() => this.setEditMode(index)}
                            />
                        ))}
                    </section>
                    <Footer>
                        <Button
                            icon="🙋‍♀️"
                            disabled={!this.canAddNewPlayer()}
                            onClick={this.addPlayer}
                        >
                            Add new player
                        </Button>
                        <Button icon="🏆" onClick={this.findWinner}>
                            Find the winner
                        </Button>
                    </Footer>
                </section>
            </Layout>
        );
    }
}

export default App;
